/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.kitiwat.swingproject;

import java.io.Serializable;

/**
 *
 * @author sanak
 */
public class Friend implements Serializable{
    private String name;
    private int age;
    private String gender;
    private String des;

    public Friend(String name, int age, String gender, String des) {
        this.name = name;
        this.age = age;
        this.gender = gender;
        this.des = des;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public String getDes() {
        return des;
    }

    public void setDes(String des) {
        this.des = des;
    }

    @Override
    public String toString() {
        return "Friend{" + "name=" + name + ", age=" + age + ", gender=" + gender + ", des=" + des + '}';
    }
    
    
}
